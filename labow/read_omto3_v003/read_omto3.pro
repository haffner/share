; for gordon

; read all lines in text file
function readlines, filename
  openr,21,filename
  lines=strarr(file_lines(filename))
  readf,21,lines
  close,21
  return,lines
end

; read data from hdf5 or netCDF4 file using simple hdf5 interface
function h5_read, files, paths

  xx = list(length=paths.length)

  for i=0, xx.length-1 do $
     xx[i]=list(length=files.length)

  foreach file, files, i do $
     foreach path, paths, j do $
        xx[j,i] = h5_getdata(file, path)

  foreach path, paths, j do begin
     if stregex(path, "Wavelength",/bool) then begin
        xx[j]= xx[j,0]
     endif else begin
        xx[j] = temporary((xx[j]).toarray(dim=(xx[j,0]).ndim))
     endelse
  endforeach     

  vars = file_basename(paths)
  data={}
  foreach var, vars, j do begin
     data = create_struct(data, var, xx[j])
     xx[j]=!null
   endforeach
     
return, data     
end

; main
files=file_search("/tis/OMI/10003/OMTO3/2022/01/17/*.he5")
paths=readlines('paths_omto3.txt')
data = h5_read(files, paths)
lat=data.latitude
lon=data.longitude
oz1=data.steponeo3
res1=data.residualstep1
wav=data.wavelength
res308 = reform(res1[0,*,*])
end
